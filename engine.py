import math

"""HELPERS"""
#Linear
def linear_x(progress, kx, x_speed):
    return (kx - progress*x_speed)

def linear_y(progress, ky, y_speed):
    return (ky - progress*y_speed)

#Sine
def sine_y(progress, ky, freq):
    #print progress
    return 50*math.sin((progress*freq)/30.0) + ky

#Cosine
def cosine_x(progress, kx, freq):
        return 50*math.cos((progress*freq)/30.0) + (kx)

"""PATHS"""
def linear(progress, kx, ky, x_speed, y_speed):
    x = linear_x(progress, kx, x_speed)
    y = linear_y(progress, ky, y_speed)
    return (x, y)

def sine(progress, kx, ky, x_speed, y_speed):
    #print progress
    x = linear_x(progress, kx, x_speed)
    y = sine_y(progress, ky, y_speed)
    #print progress
    return (x, y)

def swing(progress, kx, ky, x_speed, y_speed):
    x = cosine_x(progress, kx, x_speed) - progress
    y = sine_y(progress, ky, y_speed)
    return (x, y)
