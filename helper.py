import pygame, random
import settings as st
pygame.init()


def get_random_coordinates(width, height):
    """
    Returns a random 2D coordinate as a 2-tuple
    """
    rand_x = random.randint(width, st.WINDOW_WIDTH-width)
    rand_y = random.randint(height, st.WINDOW_HEIGHT-height)
    return (rand_x, rand_y)

def check_collision(rect_1, rect_2):
    """
    Returns True if both rectangles are colliding with each other
    """
    dx = abs(rect_1.centerx - rect_2.centerx)
    dy  = abs(rect_1.centery - rect_2.centery)
    return dx < (rect_1.width + rect_2.width)/2 and dy < (rect_1.height + rect_2.height)/2

#---Movement-Management(Boundary and hero movement constraint)

#---BOUNDARY---
def locate_in_boundary(target, boundary):
    b_top = target.top - boundary.top
    b_bottom = target.bottom - boundary.bottom
    b_left = target.left - boundary.left
    b_right = target.right - boundary.right

    TOP = (b_top <= 0 )
    BOTTOM = (b_bottom >= 0 )
    LEFT = (b_left <= 0 )
    RIGHT = (b_right >= 0 )

    if TOP and RIGHT:
        return "TOP_RIGHT"
    elif TOP and LEFT:
        return "TOP_LEFT"
    elif BOTTOM and RIGHT:
        return "BOTTOM_RIGHT"
    elif BOTTOM and LEFT:
        return "BOTTOM_LEFT"
    elif TOP:
        return "TOP"
    elif BOTTOM:
        return "BOTTOM"
    elif LEFT:
        return "LEFT"
    elif RIGHT:
        return "RIGHT"
    else: 
        return "BOUNDED"


#movement manager (hero) 
def movement_manager(target, loc_string):

    if loc_string == "TOP":
        if target.dir_y < 0:
            target.dir_y = 0
    if loc_string == "BOTTOM":
        if target.dir_y > 0:
            target.dir_y = 0
    if loc_string == "LEFT":
        if target.dir_x < 0:
            target.dir_x = 0
    if loc_string == "RIGHT":
        if target.dir_x > 0:
            target.dir_x = 0
    if loc_string == "TOP_RIGHT":
        if (target.dir_y < 0) and (target.dir_x > 0):
            target.dir_y = 0
            target.dir_x = 0
        if target.dir_y < 0:
            target.dir_y = 0
        if target.dir_x > 0:
            target.dir_x = 0
    if loc_string == "TOP_LEFT":
        if (target.dir_y < 0) and (target.dir_x < 0):
            target.dir_y = 0
            target.dir_x = 0
        if target.dir_y < 0:
            target.dir_y = 0
        if target.dir_x < 0:
            target.dir_x = 0
    if loc_string == "BOTTOM_RIGHT":
        if (target.dir_y > 0) and (target.dir_x > 0):
            target.dir_y = 0
            target.dir_x = 0
        if target.dir_y > 0:
            target.dir_y = 0
        if target.dir_x > 0:
            target.dir_x = 0
    if loc_string == "BOTTOM_LEFT":
        if (target.dir_y > 0) and (target.dir_x < 0):
            target.dir_y = 0
            target.dir_x = 0
        if target.dir_y > 0:
            target.dir_y = 0
        if target.dir_x < 0:
            target.dir_x = 0
    
    
