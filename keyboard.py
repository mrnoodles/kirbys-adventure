#keyboard.py

""" Keyboard Handler """

import pygame
from pygame.locals import *
pygame.init()

question = {
    "YES":K_y,
    "NO":K_n
}

move = {
    "UP":K_UP,
    "DOWN":K_DOWN,
    "LEFT":K_LEFT,
    "RIGHT":K_RIGHT
    }

sys = {
    "MUTE":K_m,
    "=":K_EQUALS,
    "K+":K_KP_PLUS,
    "-":K_MINUS,
    "K-":K_KP_MINUS,
    "PAUSE":K_p,
    "QUIT":K_q,
    "ESCAPE":K_ESCAPE
    }

act = {
    "LASER":K_SPACE,
    "PEW":K_x
    }

directions = {
    "NONE": (0,0),
    "UP": (0,-1),
    "DOWN": (0,1),
    "LEFT": (-1,0),
    "RIGHT": (1,0),
    "TOP_LEFT": (-1,-1),
    "TOP_RIGHT": (1,-1),
    "BOTTOM_LEFT": (-1,1),
    "BOTTOM_RIGHT": (1,1)
    }

def escape_prompt(event, gameover=False):
    if not gameover:
        return (event.type == QUIT or (event.type == KEYDOWN and event.key == sys["ESCAPE"]))
    else:
        return (event.type == QUIT or (event.type == KEYDOWN and event.key == sys["ESCAPE"]) or (event.type == KEYDOWN and event.key == question["NO"]))

def restart(event, gameover=False):
    if gameover:
        return event.type == KEYDOWN and event.key == question["YES"]
    else:
        return False

def pause_prompt(keys):
    return keys[sys["PAUSE"]]

def music_prompt(keys):
    return keys[sys["MUTE"]]

def increase_prompt(keys):
    return keys[sys["K+"]] or keys[sys["="]]

def decrease_prompt(keys):
    return keys[sys["-"]] or keys[sys["K-"]]

def laser_prompt(keys):
    if keys[act["LASER"]]:
        return True
    return False

def pew_prompt(keys):
    if keys[act["PEW"]]:
        return True
    return False

def movement(keys):
    direction = directions["NONE"]
    for d in move:
        if keys[move[d]]:
            direction = directions[d]
    #DAT DIAG
    if (keys[move["UP"]] and keys[move["LEFT"]]):
        direction = directions["TOP_LEFT"]
    if (keys[move["UP"]] and keys[move["RIGHT"]]):
        direction = directions["TOP_RIGHT"]
    if (keys[move["DOWN"]] and keys[move["LEFT"]]):
        direction = directions["BOTTOM_LEFT"]
    if (keys[move["DOWN"]] and keys[move["RIGHT"]]):
        direction = directions["BOTTOM_RIGHT"]
    
    return direction
